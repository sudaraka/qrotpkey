 # Makefile: build script
 #
 # Copyright 2018 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 #
 # This program comes with ABSOLUTELY NO WARRANTY
 # This is free software, and you are welcome to redistribute it and/or modify
 # it under the terms of the BSD 2-clause License. See the LICENSE file for more
 # details.
 #

prefix ?=/usr/local
bindir ?=${prefix}/bin
target ?=node10-linux-x64

srcdir=src
BUILD_DIR=build
BINARY ?=$(shell npm run show-name --silent)
VERSION ?=$(shell npm run show-version --silent)
EXECUTABLE=${BUILD_DIR}/${BINARY}-${VERSION}

all: ${EXECUTABLE}
	@echo -e "\nBuild complete."

${EXECUTABLE}: ${srcdir}/index.js package.json package-lock.json ${BUILD_DIR} node_modules/**/*
	npx pkg -t ${target} --options no-warnings -o $@ .
	chmod +x $@

${BUILD_DIR}:
	mkdir -p $@

install: ${EXECUTABLE}
	install -d -m 0755 ${bindir}
	install -m 0755 ${EXECUTABLE} ${bindir}

	ln -s ./${BINARY}-${VERSION} ${bindir}/${BINARY}

	@echo -e "\nInstallation complete."

uninstall:
	test -d ${bindir} && \
	cd ${bindir} && \
	$(RM) ${BINARY} && \
	$(RM) ${BINARY}-*

	@echo -e "\nUninstall complete."

clean:
	$(RM) -r ${BUILD_DIR}

.PHONY: all clean install uninstall
