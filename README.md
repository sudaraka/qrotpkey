# QR OTP Key

Read and display [OTP](https://en.wikipedia.org/wiki/One-time_password) keys
from [QR code](https://en.wikipedia.org/wiki/QR_code) in a
[PNG](https://en.wikipedia.org/wiki/Portable_Network_Graphics) file.

## License

Copyright 2018 Sudaraka Wijesinghe <sudaraka@sudaraka.org>

This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it and/or modify it
under the terms of the BSD 2-clause License. See the LICENSE file for more
details.
